//
//  UsersListViewController.swift
//  UsersApp
//
//  Created by Deepak kumar on 23/11/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import UIKit

class UsersListViewController: UIViewController {
    let cellIdentifier = "userCell"
    
    @IBOutlet weak var tableView: UITableView!
    var refreshControl = UIRefreshControl()
    
    var users: [UserModel] = []{
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.refreshData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getUsersList(limit: Constants.initialLimit)
    }
    
    func setupUI() {
        title = StringConstants.usersListTitle
        
        refreshControl.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        tableView.refreshControl = refreshControl
        
        tableView.estimatedRowHeight = 100
    }
    
    func getEndPoint(limit: Int) -> String {
        let url = URLBuilder(baseUrl: ApiUrl.baseUrl)
        url.addQueryParameter(paramKey: RequestKeys.results, value: "\(limit)")
        return url.getFinalUrl()
    }

    func getUsersList(limit: Int) {
        let httpReq = HTTPClient(url: getEndPoint(limit: limit))
        httpReq.sendRequest {[weak self] (data, error) in
            guard let weakSelf = self else {
                return
            }
            weakSelf.hideLoader()
            
            if let response = data {
                do {
                    let decoder = JSONDecoder()
                    let result: [String: Any] = try JSONSerialization.jsonObject(with: response) as! [String : Any]
                    let userData = try JSONSerialization.data(withJSONObject: result[RequestKeys.results], options: .prettyPrinted)
                    let newList = try decoder.decode([UserModel].self, from: userData)
                    weakSelf.users.insert(contentsOf: newList, at: 0)
                } catch {
                    
                }
            }
        }
    }
    
    @objc func refreshList() {
        getUsersList(limit: Constants.refreshLimit)
    }
    
    func refreshData() {
        tableView.reloadData()
    }
    
    func hideLoader() {
        DispatchQueue.main.async { [weak self] in
            self?.refreshControl.endRefreshing()
        }
    }

}

extension UsersListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UserViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! UserViewCell
        cell.configureCellWith(user: users[indexPath.row])
        cell.selectionStyle = .none
        
        return cell
    }
}

extension UsersListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "UserDetailsViewController") as! UserDetailsViewController
        userDetailVC.mapViewModel = users[indexPath.row]
        navigationController?.pushViewController(userDetailVC, animated: true)
    }
}
