//
//  UserDetailsViewController.swift
//  UsersApp
//
//  Created by Deepak kumar on 24/11/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import UIKit
import MapKit

class UserDetailsViewController: UIViewController {
    @IBOutlet weak var mapView: MKMapView!
    let visibilityDistnace = 1000.0
    
    let annotationIdentifier = "marker"
    var mapViewModel: UserModel!

    init(detailViewModel: UserModel) {
        super.init(nibName: nil, bundle: nil)
        mapViewModel = detailViewModel
    }

    func deliveryLocation() -> CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: Double(mapViewModel.location?.coordinates?.latitude ?? "0.0")!, longitude: Double(mapViewModel.location?.coordinates?.longitude ?? "0.0")!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }

    func setupUI() {
        title = StringConstants.userDetailsTitle
        view.backgroundColor = .white

        mapView.showsUserLocation = true
        let pinLocation: CLLocationCoordinate2D = deliveryLocation()
        
        let deliveryPin = CustomMarker.init(withTitle: mapViewModel.location?.getAddress() ?? "", coordinate: pinLocation)
        mapView.addAnnotation(deliveryPin)

        let viewRegion = MKCoordinateRegion(center: pinLocation, latitudinalMeters: visibilityDistnace, longitudinalMeters: visibilityDistnace)
        mapView.setRegion(viewRegion, animated: true)
    }
}

extension UserDetailsViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var view: MKMarkerAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
            as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            view.canShowCallout = true
            view.detailCalloutAccessoryView = UIView()
        }
        return view
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {

    }
}
