//
//  HTTPClient.swift
//  UsersApp
//
//  Created by Deepak kumar on 22/11/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import UIKit

enum ApiType: String {
    case GET
    case POST
}

class HTTPClient: HTTPClientProtocol {
    var url: String
    var requestJson: [String: String]?
    var session = URLSession.shared

    init(url: String, requestJson: [String: String] = [:]) {
        self.url = url
        self.requestJson = requestJson
    }

    func sendRequest(completionHandler: @escaping CompletionBlock) {
        let request: NSMutableURLRequest = NSMutableURLRequest(url: URL(string: url)!, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: Constants.networkTimeoutInterval)
        request.httpMethod = ApiType.GET.rawValue

        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, _, error -> Void in
            completionHandler(data, error)
        })

        task.resume()
    }
}
