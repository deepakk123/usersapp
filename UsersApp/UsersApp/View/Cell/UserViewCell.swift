//
//  UserViewCell.swift
//  UsersApp
//
//  Created by Deepak kumar on 23/11/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import UIKit

class UserViewCell: UITableViewCell {
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    
    let imgCornerRadius:CGFloat = 10.0
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        profileImgView.layer.cornerRadius = profileImgView.frame.height / 2.0
    }
        
    func configureCellWith(user: UserModel) {
        nameLbl.text = user.name?.getFullName()
        phoneLbl.text = user.phone
        locationLbl.text = user.location?.getAddress()
        
        if let imgUrl = user.picture?.thumbnail {
            ImageCache.shared.getImageFrom(urlStr: imgUrl) { (image) in
                DispatchQueue.main.async {
                    self.profileImgView.image = image
                }
            }
        }
    }

}
