//
//  UserModel.swift
//  UsersApp
//
//  Created by Deepak kumar on 22/11/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import Foundation

struct UserModel: Codable {
    let gender: String?
    let name: NameModel?
    let location: LocationModel?
    let email: String?
    let login: LoginModel?
    let dob: DobModel?
    let registered: DobModel?
    let phone: String?
    let cell: String?
    let id: IdModel?
    let picture: PictureModel?
    let nat: String?
}
    
struct NameModel: Codable {
    let title: String?
    let first: String?
    let last: String?
    
    func getFullName() -> String {
        let strBuilder = StringBuilder(baseStr: title)
        strBuilder.addParam(val: first, separator: " ")
        strBuilder.addParam(val: last, separator: " ")
        
        return strBuilder.finalStr
    }
}

struct LocationModel: Codable {
    let street: StreetModel?
    let city: String?
    let state: String?
    let country: String?
    //let postcode: Int?
    let coordinates: CoordinatesModel?
    let timezone: TimeZoneModel?
    
    func getAddress() -> String {
        let strBuilder = StringBuilder(baseStr: street?.getAddress())
        strBuilder.addParam(val: city, separator: ", ")
        strBuilder.addParam(val: state, separator: ", ")
        strBuilder.addParam(val: country, separator: ", ")
        
        return strBuilder.finalStr
    }
}

struct CoordinatesModel: Codable {
    let latitude: String?
    let longitude: String?
}

struct StreetModel: Codable {
    let number: Int?
    let name: String?
    
    func getAddress() -> String {
        var address = ""
        if let num = number {
            address += "\(num), "
        }
        return address  + (name ?? "")
    }
}

struct TimeZoneModel: Codable {
    let offset: String?
    let description: String?
}

struct LoginModel: Codable {
    let uuid: String?
    let username: String?
    let password: String?
    let salt: String?
    let md5: String?
    let sha1: String?
    let sha256: String?
}

struct DobModel: Codable {
    let date: String?
    let age: Int?
}

struct IdModel: Codable {
    let name: String?
    let value: String?
}

struct PictureModel: Codable {
   let large: String?
   let medium: String?
   let thumbnail: String?
}


class StringBuilder: NSObject {
    var finalStr = ""
    
    init(baseStr: String?) {
        finalStr = baseStr ?? ""
    }
    
    func addParam(val: String?, separator: String = "") {
        guard let val = val else {
            return
        }
        
        if finalStr.isEmpty {
            finalStr = val
        } else {
            finalStr += separator + val
        }
        
    }
}
