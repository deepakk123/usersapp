//
//  HTTPClientProtocol.swift
//  UsersApp
//
//  Created by Deepak kumar on 22/11/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import Foundation

protocol HTTPClientProtocol {
    typealias CompletionBlock = (_ response: Data?, _ error: Error?) -> Void
    func sendRequest(completionHandler: @escaping CompletionBlock)
}
