//
//  Constants.swift
//  UsersApp
//
//  Created by Deepak kumar on 22/11/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import Foundation

struct ApiUrl {
    static let baseUrl = "https://randomuser.me/api/"
}

struct Constants {
    static let networkTimeoutInterval: TimeInterval = 10
    static let initialLimit = 5
    static let refreshLimit = 3
}

struct RequestKeys {
    static let results    = "results"
}

struct StringConstants {
    static let usersListTitle = "Users"
    static let userDetailsTitle = "User Details"
    static let okTitle = "Ok"
    static let noDataErrorMsg = "Please check your internet connection and pull down to refresh the list."
}
