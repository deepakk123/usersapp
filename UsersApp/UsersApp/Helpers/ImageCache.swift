//
//  ImageCache.swift
//  UsersApp
//
//  Created by Deepak kumar on 24/11/19.
//  Copyright © 2019 Deepak kumar. All rights reserved.
//

import Foundation
import UIKit

class ImageCache: NSObject {
    static let shared = ImageCache()
    let imageCache = NSCache<NSString, UIImage>()
    
    func getImageFrom(urlStr: String, imageCompletion:@escaping ((UIImage?)->Void)) {
        if let cachedImage = imageCache.object(forKey: urlStr as NSString) {        //Image is already downloaded
            imageCompletion(cachedImage)
        }else{
            //Make network call to get the image
            DispatchQueue.global(qos: .background).async {
                if let url = URL(string:urlStr), let data = try? Data(contentsOf: url) {
                    let image: UIImage = UIImage(data: data)!
                    
                    DispatchQueue.main.async {
                        self.imageCache.setObject(image, forKey: urlStr as NSString)
                        imageCompletion(image)
                    }
                } else {
                    imageCompletion(nil)
                }
            }
        }
    }
    
}
